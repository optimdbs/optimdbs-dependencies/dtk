CHANGELOG.txt --- 
;; 
;; Author: Julien Wintz
;; Copyright (C) 2008-2011 - Julien Wintz, Inria.
;; Created: Thu Jun 30 00:13:01 2011 (+0200)
;; Version: $Id$
;; Last-Updated: Thu Sep  8 09:41:35 2011 (+0200)
;;           By: jwintz
;;     Update #: 12
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Commentary: 
;; This CHANGELOG lists external functionalities contributed by the
;;   mean of merge requests.
;;
;; It does NOT include minor changes operated by the team of AUTHORS.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Change log:
;; 8-Sep-2011     Julien Wintz
;;    Last-Updated: Thu Sep  8 09:40:43 2011 (+0200) #10 (Julien Wintz)
;;    0.6.0: Added Json layer. (nniclaus)
;; 8-Sep-2011     Julien Wintz
;;    Last-Updated: Thu Sep  8 09:39:21 2011 (+0200) #8 (Julien Wintz)
;;    0.5.3: Added procompiled header support. (jstark)
;; 8-Sep-2011     Julien Wintz
;;    Last-Updated: Thu Sep  8 09:36:27 2011 (+0200) #5 (Julien Wintz)
;;    0.5.2: Introduction of dtk singleton. (jstark)
;; 5-Jul-2011     Julien Wintz  
;;    Last-Updated: Thu Jun 30 00:13:47 2011 (+0200) #3 (Julien Wintz)
;;    0.5.1: Better handling of smart pointers in core layer. (jstark)
;; 30-Jun-2011    Julien Wintz  
;;    Last-Updated: Thu Jun 30 00:13:13 2011 (+0200) #1 (Julien Wintz)
;;    0.5.0: Initial version. See git logs for details.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
